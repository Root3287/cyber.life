# CyberLife
A social platform in a 3D world.

---
## Getting started
Start by cloning the repository with:
```
git clone --recursive https://gitlab.com/HerrDingenz/cyber.life
```
**IMPORTANT NOTE**: If you didn't clone the repository recursively you have to run the Get_Or_Update_Submodules script **BEFORE** generating the solution and project files!!! Otherwise you will have to run the project generation script again.

## Windows
If you didn't clone the repository recursively (or you just want to update the submodules being used) go to the Scripts folder and run:
```
Get_Or_Update_Submodules.bat
```
To generate the Visual Studio 2019 solution and project files go to the Scripts folder and run:
```
Generate_VS2019_Solution.bat
```
## Linux
If you didn't clone the repository recursively (or you just want to update the submodules being used) go to the Scripts folder and run:
```
Get_Or_Update_Submodules.sh
```
To generate the gmake2 solution and project files go to the Scripts folder and run:
```
Generate_Make_Solution.sh
```
