-- include directories relative to root folder (solution directory) ------------
IncludeDir = {}
IncludeDir["clcl"] = "Cyber.Life.CoreLibrary"
IncludeDir["spdlog"] = "Vendors/spdlog/include"

-- main workspace --------------------------------------------------------------
workspace "Cyber.Life"
	architecture "x86_64"
	startproject "Cyber.Life"

	outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"
	targetdir (".build/Binaries/" .. outputdir .. "/%{prj.name}")
	objdir (".build/Intermediates/" .. outputdir .. "/%{prj.name}")

	configurations
	{
		"Debug",
		"RC",
		"Release"
	}

	flags
	{
		"MultiProcessorCompile"
	}

	filter "system:windows"
		systemversion "latest"
		entrypoint "mainCRTStartup"
		staticruntime "On"

		defines
		{
			"CL_PLATFORM_WINDOWS"
		}

	filter "system:linux"
		defines
		{
			"CL_PLATFORM_LINUX"
		}

		libdirs
		{
			"/usr/lib/x86_64-linux-gnu"
		}

	filter "system:macosx"
		defines 
		{
			"CL_PLATFORM_MACOS"
		}
		
    filter "configurations:Debug"
        symbols "On"
        runtime "Debug"

        defines
		{
			"CL_DEBUG",
			"CL_PROFILING"
		}

    filter "configurations:RC"
        optimize "On"
        runtime "Release"

        defines
		{
			"CL_RELEASECANDIDATE",
			"CL_PROFILING"
		}

    filter "configurations:Release"
        optimize "On"
        runtime "Release"

		defines
		{
			"CL_RELEASE"
		}

-- Cyber.Life ------------------------------------------------------------------
project "Cyber.Life"
	location "Cyber.Life"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"

	pchheader "pch.hpp"
	pchsource "%{prj.location}/pch.cpp"

	files
	{
		"%{prj.location}/**.hpp",
		"%{prj.location}/**.cpp"
	}

	includedirs
	{
		"%{prj.location}",
		"%{IncludeDir.clcl}",
		"%{IncludeDir.spdlog}"
	}

	dependson
	{
		"Cyber.Life.CoreLibrary"
	}

	links
	{
		"Cyber.Life.CoreLibrary"
	}

	filter "system:linux"
		links
		{
			"dl",
			"pthread"
		}

	filter {"system:windows", "configurations:Debug"}
		linkoptions "/SUBSYSTEM:CONSOLE"

	filter {"system:windows", "configurations:RC"}
		linkoptions "/SUBSYSTEM:WINDOWS"

	filter {"system:windows", "configurations:Release"}
		linkoptions "/SUBSYSTEM:WINDOWS"

-- Cyber.Life.CoreLibrary ------------------------------------------------------
project "Cyber.Life.CoreLibrary"
	location "Cyber.Life.CoreLibrary"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"

	pchheader "pch.hpp"
	pchsource "%{prj.location}/pch.cpp"

	files
	{
		"%{prj.location}/**.hpp",
		"%{prj.location}/**.cpp"
	}

	includedirs
	{
		"%{prj.location}",
		"%{IncludeDir.spdlog}"
	}

	filter "system:linux"
		links
		{
			"dl",
			"pthread"
		}

	filter {"system:windows", "configurations:Debug"}
		linkoptions "/SUBSYSTEM:CONSOLE"

	filter {"system:windows", "configurations:RC"}
		linkoptions "/SUBSYSTEM:WINDOWS"

	filter {"system:windows", "configurations:Release"}
		linkoptions "/SUBSYSTEM:WINDOWS"