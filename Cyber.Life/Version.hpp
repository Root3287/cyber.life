// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#define CL_APPLICATION_NAME (const std::string)"Cyber.Life"
#define CL_APPLICATION_VERSION CL_MAKE_VERSION(0, 0, 0)
#define CL_APPLICATION_VERSION_SUFFIX (const std::string)"a"
#define CL_APPLICATION_VERSION_STRING								\
	(const std::string)("v"											\
		+ std::to_string(CL_VERSION_MAJOR(CL_APPLICATION_VERSION))	\
		+ "."														\
		+ std::to_string(CL_VERSION_MINOR(CL_APPLICATION_VERSION))	\
		+ "."														\
		+ std::to_string(CL_VERSION_PATCH(CL_APPLICATION_VERSION))	\
		+ CL_APPLICATION_VERSION_SUFFIX)