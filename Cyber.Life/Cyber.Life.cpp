// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "pch.hpp"

int main()
{
	CLCL::Logging::Initialize(CL_APPLICATION_NAME + " " + CL_APPLICATION_VERSION_STRING);
	CL_LOG_TRACE("Welcome to {0} {1} running on {2} {3}.", CL_APPLICATION_NAME, CL_APPLICATION_VERSION_STRING, CL_LIBRARY_NAME, CL_LIBRARY_VERSION_STRING);

	CL_PROFILING_BEGIN_SESSION("Init", CL_APPLICATION_NAME + " " + CL_APPLICATION_VERSION_STRING + "-Init");
	// TODO: initialize stuff here
	CL_PROFILING_END_SESSION();

	CL_PROFILING_BEGIN_SESSION("Loop", CL_APPLICATION_NAME + " " + CL_APPLICATION_VERSION_STRING + "-Loop");
	// TODO: application loop
	CL_PROFILING_END_SESSION();

	CL_PROFILING_BEGIN_SESSION("Shutdown", CL_APPLICATION_NAME + " " + CL_APPLICATION_VERSION_STRING + "-Shutdown");
	// TODO: shut down stuff here
	CL_PROFILING_END_SESSION();

	CL_LOG_TRACE("See you again in {0} {1}.", CL_APPLICATION_NAME, CL_APPLICATION_VERSION_STRING);
	std::cin.get(); // TODO: remove this at some point
	return 0;
}