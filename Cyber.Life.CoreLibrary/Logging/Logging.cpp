// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "pch.hpp"

namespace CLCL
{
	void Logging::Initialize(const std::string& filePath /*= "log"*/) noexcept
	{
		if (!s_FileLogger)
		{
			s_FileLogger = spdlog::basic_logger_mt("FILE", filePath + ".log", true);
			s_FileLogger->set_pattern("%L: %v");
			s_FileLogger->set_level(spdlog::level::trace);
			s_FileLogger->flush_on(spdlog::level::trace);
		}

		#if defined(CL_DEBUG)
			if (!s_ConsoleLogger)
			{
				s_ConsoleLogger = spdlog::stdout_color_mt("DBEUG");
				s_ConsoleLogger->set_pattern("%^[%T]: %v%$");
				s_ConsoleLogger->set_level(spdlog::level::trace);
			}
		#endif

		CL_LOG_DEBUG("Logging initialized.");
	}

	const std::shared_ptr<spdlog::logger>& Logging::GetFileLogger() noexcept
	{
		if (!s_FileLogger)
		{
			Initialize();
		}

		return s_FileLogger;
	}

	#if defined(CL_DEBUG)
		const std::shared_ptr<spdlog::logger>& Logging::GetConsoleLogger() noexcept
		{
			if (!s_ConsoleLogger)
			{
				Initialize();
			}

			return s_ConsoleLogger;
		}
	#endif
}