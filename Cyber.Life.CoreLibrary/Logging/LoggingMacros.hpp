// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#if defined(CL_DEBUG)
	#define CL_LOG_TRACE(...)									\
		CLCL::Logging::GetConsoleLogger()->trace(__VA_ARGS__);	\
		CLCL::Logging::GetFileLogger()->trace(__VA_ARGS__)
	#define CL_LOG_DEBUG(...)									\
		CLCL::Logging::GetConsoleLogger()->debug(__VA_ARGS__);	\
		CLCL::Logging::GetFileLogger()->debug(__VA_ARGS__)
	#define CL_LOG_INFO(...)									\
		CLCL::Logging::GetConsoleLogger()->info(__VA_ARGS__);	\
		CLCL::Logging::GetFileLogger()->info(__VA_ARGS__)
	#define CL_LOG_WARN(...)									\
		CLCL::Logging::GetConsoleLogger()->warn(__VA_ARGS__);	\
		CLCL::Logging::GetFileLogger()->warn(__VA_ARGS__)
	#define CL_LOG_ERROR(...)									\
		CLCL::Logging::GetConsoleLogger()->error(__VA_ARGS__);	\
		CLCL::Logging::GetFileLogger()->error(__VA_ARGS__)
	#define CL_LOG_CRITICAL(...)									\
		CLCL::Logging::GetConsoleLogger()->critical(__VA_ARGS__);	\
		CLCL::Logging::GetFileLogger()->critical(__VA_ARGS__)
#elif defined(CL_RELEASECANDIDATE)
	#define CL_LOG_TRACE(...)									\
		CLCL::Logging::GetFileLogger()->trace(__VA_ARGS__)
	#define CL_LOG_DEBUG(...)									\
		CLCL::Logging::GetFileLogger()->debug(__VA_ARGS__)
	#define CL_LOG_INFO(...)									\
		CLCL::Logging::GetFileLogger()->info(__VA_ARGS__)
	#define CL_LOG_WARN(...)									\
		CLCL::Logging::GetFileLogger()->warn(__VA_ARGS__)
	#define CL_LOG_ERROR(...)									\
		CLCL::Logging::GetFileLogger()->error(__VA_ARGS__)
	#define CL_LOG_CRITICAL(...)								\
		CLCL::Logging::GetFileLogger()->critical(__VA_ARGS__)
#elif defined(CL_RELEASE)
	#define CL_LOG_TRACE(...)									\
		CLCL::Logging::GetFileLogger()->trace(__VA_ARGS__)
	#define CL_LOG_DEBUG(...)
	#define CL_LOG_INFO(...)									\
		CLCL::Logging::GetFileLogger()->info(__VA_ARGS__)
	#define CL_LOG_WARN(...)									\
		CLCL::Logging::GetFileLogger()->warn(__VA_ARGS__)
	#define CL_LOG_ERROR(...)									\
		CLCL::Logging::GetFileLogger()->error(__VA_ARGS__)
	#define CL_LOG_CRITICAL(...)								\
		CLCL::Logging::GetFileLogger()->critical(__VA_ARGS__)
#endif