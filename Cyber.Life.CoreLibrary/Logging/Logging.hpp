// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#include "Logging/LoggingMacros.hpp"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace CLCL
{
	class Logging
	{
	public:
		Logging() noexcept = delete;
		Logging(const Logging&) noexcept = delete;
		Logging(Logging&&) noexcept = delete;
		Logging& operator=(const Logging&) noexcept = delete;
		Logging& operator=(Logging&&) noexcept = delete;
		~Logging() noexcept = delete;

		//************************************
		// Initializes the logging system which uses spdlog (https://github.com/gabime/spdlog).
		// Use the macros defined in Logging/LoggingMacros.hpp to log messages.
		// The file logger outputs to the file given with the file path (the extension ".log" gets added autmatically) and is available in all builds.
		// The console logger outputs to the console/terminal and is only available in Debug and RC builds.
		// Method:    Initialize
		// FullName:  CLCL::Logging::Initialize
		// Access:    public static 
		// Returns:   void
		// Qualifier: noexcept
		// Parameter: const std::string& filePath
		//************************************
		static void Initialize(const std::string& filePath = "log") noexcept;

		//************************************
		// Returns the file logger.
		// Available in all builds.
		// If logging is not initialized yet it will initialize the logging system and return the file logger after initialization.
		// NOTE: In that case the file logger will be initialized with the standard file name "log.log" in the current working directory!!!
		// Method:    GetFileLogger
		// FullName:  CLCL::Logging::GetFileLogger
		// Access:    public static 
		// Returns:   const std::shared_ptr<spdlog::logger>&
		// Qualifier: noexcept
		//************************************
		[[nodiscard]] static const std::shared_ptr<spdlog::logger>& GetFileLogger() noexcept;

		inline static std::shared_ptr<spdlog::logger> s_FileLogger = nullptr;

		#if defined(CL_DEBUG)
			//************************************
			// Returns the console logger.
			// Only available in Debug builds.
			// If logging is not initialized yet it will initialize the logging system and return the console logger after initialization.
			// NOTE: In that case the file logger will be initialized with the standard file name "log.log" in the current working directory!!!
			// Method:    GetConsoleLogger
			// FullName:  CLCL::Logging::GetConsoleLogger
			// Access:    public static
			// Returns:   const std::shared_ptr<spdlog::logger>&
			// Qualifier: noexcept
			//************************************
			[[nodiscard]] static const std::shared_ptr<spdlog::logger>& GetConsoleLogger() noexcept;

			inline static std::shared_ptr<spdlog::logger> s_ConsoleLogger = nullptr;
		#endif
	};
}