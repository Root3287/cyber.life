// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "pch.hpp"

CLCL::Profiling::~Profiling() noexcept
{
	if (m_CurrentSession)
	{
		EndSession();
	}
}

CLCL::Profiling& CLCL::Profiling::Get() noexcept
{
	static Profiling instance;
	return instance;
}

void CLCL::Profiling::BeginSession(const std::string& sessionName, const std::string& filePath /*= "profiling_results"*/)
{
	if (m_CurrentSession)
	{
		CL_LOG_WARN("Profiling session '{0}' already opened - closing it before opening profiling session '{1}'!", m_CurrentSession->SessionName, sessionName);
		EndSession();
	}

	{
		std::scoped_lock lock(m_Mutex);
		m_OutputStream.open(filePath + ".json", std::ios::out | std::ios::trunc);

		if (!m_OutputStream.is_open())
		{
			CL_LOG_WARN("Could not open results file '{0}' for profiling session '{1}'!", filePath, sessionName);
			return;
		}

		m_CurrentSession = new ProfilingSession({ sessionName });
		m_OutputStream << "{\"otherData\": {},\"traceEvents\":[{}";
		m_OutputStream.flush();
	}

	CL_LOG_DEBUG("Profiling session '{0}' opened.", m_CurrentSession->SessionName);
}

void CLCL::Profiling::EndSession() noexcept
{
	if (!m_CurrentSession)
	{
		CL_LOG_WARN("No profiling session found to close!");
		return;
	}

	{
		std::scoped_lock lock(m_Mutex);
		m_OutputStream << "]}";
		m_OutputStream.flush();
		m_OutputStream.close();
	}

	CL_LOG_DEBUG("Profiling session {0} closed.", m_CurrentSession->SessionName);
	delete m_CurrentSession;
	m_CurrentSession = nullptr;
}

void CLCL::Profiling::WriteResult(const ProfilingResult& result) noexcept
{
	if (!m_CurrentSession)
	{
		CL_LOG_WARN("No profiling session found to write results!");
		return;
	}

	std::stringstream ss;
	ss << std::setprecision(3)
		<< std::fixed
		<< ",{\"cat\":\"function\",\"dur\":"
		<< result.ElapsedTime.count()
		<< ",\"name\":\""
		<< result.ResultName
		<< "\",\"ph\":\"X\",\"pid\":0,\"tid\":"
		<< result.ThreadID
		<< ",\"ts\":"
		<< result.StartTime.count()
		<< "}";

	std::scoped_lock lock(m_Mutex);
	m_OutputStream << ss.str();
	m_OutputStream.flush();
}