// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#if defined(CL_PROFILING)

	#if defined(_MSC_VER)
		#define CL_FUNCTION_SIGNATURE __FUNCSIG__
	#else
		#define CL_FUNCTION_SIGNATURE __PRETTY_FUNCTION__
	#endif

	#define CL_PROFILING_BEGIN_SESSION(sessionName, filePath)		\
		CLCL::Profiling::Get().BeginSession(sessionName, filePath);
	#define CL_PROFILING_END_SESSION()		\
		CLCL::Profiling::Get().EndSession();
	#define CL_PROFILING_SCOPE(scopeName)											\
		constexpr auto fixedName = CLCL::ProfilingUtil::FixResultName(scopeName);	\
		CLCL::ProfilingTimer timer(fixedName.FixedName)
	#define CL_PROFILING_FUNCTION() CL_PROFILING_SCOPE(CL_FUNCTION_SIGNATURE)
#else
	#define CL_PROFILING_BEGIN_SESSION(sessionName, filePath)
	#define CL_PROFILING_END_SESSION()
	#define CL_PROFILING_SCOPE(scopeName)
	#define CL_PROFILING_FUNCTION()
#endif