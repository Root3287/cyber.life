// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

namespace CLCL
{
	class ProfilingTimer
	{
	public:
		ProfilingTimer() noexcept = delete;

		//************************************
		// Sets the name and start time of the profiler clock.
		// Method:    ProfilingTimer
		// FullName:  CLCL::ProfilingTimer::ProfilingTimer
		// Access:    public
		// Returns:   
		// Qualifier: noexcept
		// Parameter: const char* name
		//************************************
		ProfilingTimer(const char* name) noexcept;

		ProfilingTimer(const ProfilingTimer&) noexcept = delete;
		ProfilingTimer(ProfilingTimer&&) noexcept = delete;
		ProfilingTimer& operator = (const ProfilingTimer&) noexcept = delete;
		ProfilingTimer& operator = (ProfilingTimer&&) noexcept = delete;

		//************************************
		// Calculates the elapsed time since start of the clock and triggers writing of the results to the session file.
		// Method:    ~ProfilingTimer
		// FullName:  CLCL::ProfilingTimer::~ProfilingTimer
		// Access:    public
		// Returns:   
		// Qualifier: noexcept
		//************************************
		~ProfilingTimer() noexcept;
	private:
		const char* m_Name;
		std::chrono::time_point<std::chrono::steady_clock> m_StartTimepoint;
	};
}