// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#include "Profiling/ProfilingMacros.hpp"
#include "Profiling/ProfilingTimer.hpp"
#include "Profiling/ProfilingUtil.hpp"

namespace CLCL
{
	struct ProfilingResult
	{
		std::string ResultName;
		std::chrono::duration<double, std::micro> StartTime;
		std::chrono::microseconds ElapsedTime;
		std::thread::id ThreadID;
	};

	struct ProfilingSession
	{
		std::string SessionName = "";
	};

	class Profiling
	{
		friend class ProfilingTimer; // to give access to WriteResult
	private:
		Profiling() noexcept = default;
		Profiling(const Profiling&) noexcept = delete;
		Profiling(Profiling&&) noexcept = delete;
		Profiling& operator = (const Profiling&) noexcept = delete;
		Profiling& operator = (Profiling&&) noexcept = delete;

		//************************************
		// Closes the profiling session if a session in open.
		// Desctructs the static profiling object.
		// Method:    ~Profiling
		// FullName:  CLCL::Profiling::~Profiling
		// Access:    private 
		// Returns:   
		// Qualifier: noexcept
		//************************************
		~Profiling() noexcept;
	public:
		//************************************
		// Instantiates a static profiling object and returns a reference to it.
		// Method:    Get
		// FullName:  CLCL::Profiling::Get
		// Access:    public static 
		// Returns:   CLCL::Profiling&
		// Qualifier: noexcept
		//************************************
		[[nodiscard]] static Profiling& Get() noexcept;

		//************************************
		// MUST be called first to use the profiling functionality!
		// Starts a profiling session with the session name and opens the file to output the results (the extension ".json" gets added autmatically).
		// If no file is given the profiler will generate a "profiling_results.json" file in the working directory to output profiling results.
		// Use the macros defined in Profiling/ProfilingMacros.hpp for profiling.
		// You can analyze the results by opening the results file in chrome browser using "chrome://tracing".
		// Method:    BeginSession
		// FullName:  CLCL::Profiling::BeginSession
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string& sessionName
		// Parameter: const std::string& filePath
		//************************************
		void BeginSession(const std::string& sessionName, const std::string& filePath = "profiling_results");

		//************************************
		// Ends the current profiling session if the is an active session and closes the profiling file opened by Profiling::BeginSession.
		// Use the macros defined in Profiling/ProfilingMacros.hpp for profiling.
		// You can analyze the results by opening the results file in chrome browser using "chrome://tracing".
		// Method:    EndSession
		// FullName:  CLCL::Profiling::EndSession
		// Access:    public 
		// Returns:   void
		// Qualifier: noexcept
		//************************************
		void EndSession() noexcept;
	private:
		//************************************
		// Writes the profiling result to the profiling file if a session is open.
		// Gets called by the destructor of ProfilingTimer.
		// Method:    WriteResult
		// FullName:  CLCL::Profiling::WriteResult
		// Access:    private 
		// Returns:   void
		// Qualifier: noexcept
		// Parameter: const ProfilingResult& result
		//************************************
		void WriteResult(const ProfilingResult& result) noexcept;

		ProfilingSession* m_CurrentSession = nullptr;
		std::mutex m_Mutex;
		std::ofstream m_OutputStream;
	};
}