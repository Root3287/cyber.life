// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "pch.hpp"
#include "Profiling/ProfilingTimer.hpp"

namespace CLCL
{
	ProfilingTimer::ProfilingTimer(const char* name) noexcept
		: m_Name(name), m_StartTimepoint(std::chrono::steady_clock::now())
	{}

	ProfilingTimer::~ProfilingTimer() noexcept
	{
		std::chrono::steady_clock::time_point endTimePoint = std::chrono::steady_clock::now();
		std::chrono::duration<double, std::micro> startTime = m_StartTimepoint.time_since_epoch();
		std::chrono::microseconds elapsedTime
			= std::chrono::time_point_cast<std::chrono::microseconds>(endTimePoint).time_since_epoch()
			- std::chrono::time_point_cast<std::chrono::microseconds>(m_StartTimepoint).time_since_epoch();
		Profiling::Get().WriteResult({ m_Name, startTime, elapsedTime, std::this_thread::get_id() });
	}
}