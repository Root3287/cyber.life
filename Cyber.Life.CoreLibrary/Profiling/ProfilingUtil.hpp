// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

namespace CLCL::ProfilingUtil
{
	template<size_t size>
	struct FixedResultName
	{
		char FixedName[size];
	};

	//************************************
	// Replaces " with ' in the profiler result output at compile time to prevent a corrupted JSON file.
	// Method:    FixResultName
	// FullName:  CLCL::ProfilingUtil::FixResultName
	// Access:    public 
	// Returns:   constexpr CLCL::ProfilingUtil::FixedResultName
	// Qualifier: noexcept
	// Parameter: const std::string& sourceString
	//************************************
	template<size_t size>
	constexpr FixedResultName<size> FixResultName(const char(&sourceString)[size]) noexcept
	{
		FixedResultName<size> result{};
		size_t sourceIndex = 0;
		size_t destinationIndex = 0;

		while (sourceIndex < size)
		{
			result.FixedName[destinationIndex++] = sourceString[sourceIndex] == '"' ? '\'' : sourceString[sourceIndex];
			sourceIndex++;
		}

		return result;
	}
}