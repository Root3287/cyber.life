// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#define CL_MAKE_VERSION(major, minor, patch) ((((uint32_t)(major)) << 22) | (((uint32_t)(minor)) << 12) | ((uint32_t)(patch)))
#define CL_VERSION_MAJOR(version) ((uint32_t)(version) >> 22)
#define CL_VERSION_MINOR(version) (((uint32_t)(version) >> 12) & 0x3ff)
#define CL_VERSION_PATCH(version) ((uint32_t)(version) & 0xfff)

#define CL_LIBRARY_NAME (const std::string)"CLCL"
#define CL_LIBRARY_VERSION CL_MAKE_VERSION(0, 2, 0)
#define CL_LIBRARY_VERSION_SUFFIX (const std::string)"a"
#define CL_LIBRARY_VERSION_STRING								\
	(const std::string)("v"										\
		+ std::to_string(CL_VERSION_MAJOR(CL_LIBRARY_VERSION))	\
		+ "."													\
		+ std::to_string(CL_VERSION_MINOR(CL_LIBRARY_VERSION))	\
		+ "."													\
		+ std::to_string(CL_VERSION_PATCH(CL_LIBRARY_VERSION))	\
		+ CL_LIBRARY_VERSION_SUFFIX)