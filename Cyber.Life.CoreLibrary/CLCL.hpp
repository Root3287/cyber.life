// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#pragma once

#include <fstream>
#include <iomanip>
#include <sstream>

#include "Version.hpp"
#include "Logging/Logging.hpp"
#include "Profiling/Profiling.hpp"